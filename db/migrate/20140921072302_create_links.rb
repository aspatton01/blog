class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.integer :link_id
      t.text :body

      t.timestamps
    end
  end
end
